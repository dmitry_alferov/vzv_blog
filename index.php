<?php get_header(); ?>


<div class="content">
  <div class="content-top">
    <a href="#" class="item-home desktop-hide">
      <i class="icon icon_home"></i>
    </a>
    <?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(); ?>
    <!-- breadcrumb end-->
  </div>
  <!-- content-top end-->
  <div class="articles">
    <div class="wrap-center">
      <div class="wrap-center__i clearfix">
       <?php get_sidebar(); ?>
       <!-- blog-asside end-->
       <div class="articles-content">
        <div class="blog__title">
          <div class="blog__title__name">
            <p>
              База знаний компании VZV.su
            </p>
          </div>
          <div class="blog__title__categories">

            <?php wp_nav_menu(array(
              'theme_location' => 'blog-menu',
              'items-wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>'
              )); ?>
              
            </div>
          </div>
          <?php 

          if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          <!-- post -->
          <div class="article-pewiew">
            <div class="article-pewiew__pre-title">
              <i class="icon icon-star"></i>
              <p>
                <?php the_category(', '); ?>
              </p>
            </div>
            <div class="article-pewiew__title">
              <p>
                <?php the_title(); ?>
              </p>
            </div>
            <div class="article-pewiew__tech-info">
              <ul>
                <li>
                  <i class="icon icon-person"></i>
                  <?php the_author(); ?>
                </li>
                <li class="article-date">
                  <i class="icon icon-calendar"></i>
                  <?php the_date('j F Y'); ?>
                </li>
                <li>
                  <i class="icon icon-quote"></i>
                  <?php comments_number('Нет комментариев', '1', '%'); ?>
                </li>
              </ul>
            </div>
            <div class="article-pewiew__info">
              <p>
                <?php the_excerpt(); ?>
              </p>
            </div>
            <div class="article-pewiew__image">
              <?php the_post_thumbnail('full'); ?>
            </div>
            <!-- <div class="article-pewiew__quote">
              <p>
                С каким настроением вы выйдете из магазина? Может, с таким: «Стоит ли пара-тройка часов моей жизни новой футболки?»
              </p>
            </div> -->
            <div class="article-pewiew__read-all">
              <a href=" <?php echo get_permalink(); ?> " class="btn btn_red">

                Читать далее

              </a>
            </div>
          </div>
        <?php endwhile; ?>
        <!-- post navigation -->
      <?php else: ?>
        <!-- no posts found -->
      <?php endif;

      ?>
      <div class="previous-records">
        <?php the_posts_pagination(array(
          'show_all'     => false, // показаны все страницы участвующие в пагинации
          'end_size'     => 1,     // количество страниц на концах
          'mid_size'     => 4,     // количество страниц вокруг текущей
          'prev_next'    => true,  // выводить ли боковые ссылки "предыдущая/следующая страница".
          'prev_text'    => __('« Предыдущие записи'),
          'next_text'    => __('Читать далее »'),
          'add_args'     => false, // Массив аргументов (переменных запроса), которые нужно добавить к ссылкам.
          'add_fragment' => '',     // Текст который добавиться ко всем ссылкам.
          'screen_reader_text' => __( 'Posts navigation' ),
        )); ?>
      <!-- <a href="#">
        <i class="icon icon-arrow"></i>
        Предыдущие записи
      </a> -->
    </div>
  </div>
  <!-- articles-content end-->
</div>
</div>
</div>
<!-- aricles end-->
</div>
<?php get_footer(); ?>
        <!-- content end-->