
<?php get_header(); ?>

<div class="content">
	<div class="content-top">
		<a href="#" class="item-home desktop-hide">
			<i class="icon icon_home"></i>
		</a>
		<?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(); ?>
		<!-- breadcrumb end-->
	</div>
	<!-- content-top end-->
	<div class="termins">
		<div class="wrap-center">
			<div class="wrap-center__i clearfix">
				<?php get_sidebar(); ?>

				<div class="termins-content">
					<div class="blog__title">
						<div class="blog__title__name">
							<p>
								База знаний компании VZV.su
							</p>
						</div>
						<div class="blog__title__categories">

							<?php wp_nav_menu(array(
								'theme_location' => 'blog-menu',
								'items-wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>'
								)); ?>

							</div>
						</div>
						<div class="termins-content__text">
							<?php if (have_posts()) : ?>	
						<?php while (have_posts()) : the_post(); ?>
							<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
							<h2 class="title"><?php the_title(); ?></h2>
<div class="entry">
                                
								<?php the_content('Читать далее &raquo;'); ?>
								<?php wp_link_pages(array('before' => '<p><strong>Страницы:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
							</div>
							<p class="share">Поделиться:</p>
<div class="pluso" data-background="transparent" data-options="medium,round,line,horizontal,nocounter,theme=04" data-services="vkontakte,odnoklassniki"></div>


				<?php endwhile; ?>
			
				<?php endif; ?>

							</div>
						</div>
				
					</div>
				</div>
				<!-- termins-content end-->
			</div>
		</div>
	</div>
	<!-- aricles end-->

<script type="text/javascript">(function() {
  if (window.pluso)if (typeof window.pluso.start == "function") return;
  if (window.ifpluso==undefined) { window.ifpluso = 1;
    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
    var h=d[g]('body')[0];
    h.appendChild(s);
  }})();</script>

	<?php get_footer(); ?>
