﻿<!doctype html>
<html>
<head>
<meta charset="windows-1251">
<title>VZV.SU - Спасибо!</title>
<link rel="stylesheet" href="http://www.vzv.su/polezno-znat/wp-content/themes/Solistica/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="http://www.vzv.su/polezno-znat/wp-content/themes/Solistica/css/screen.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="http://www.vzv.su/polezno-znat/wp-content/themes/Solistica/css/print.css" type="text/css" media="print" />
<script type='text/javascript' src='http://www.vzv.su/polezno-znat/wp-includes/js/jquery/jquery.js?ver=1.11.1'></script>
<script type='text/javascript' src='http://www.vzv.su/polezno-znat/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='http://www.vzv.su/polezno-znat/wp-includes/js/jquery/jquery.activity.min.js'></script>
<style type="text/css">
	div.sllogo{
		color: #7386AB;
		font-family: Tahoma;
		font-size: 11px;
	}
	body{
		
	}
	.container{
		width: 480px;
		margin: 100px auto;
	}

	p{
		font-size: 14px;
	}

	h1{
		margin-top: 40px;
	}

	.rm{
		margin-top: 40px;
	}

	a.readmore{
		font-size: 14px;
		margin-top: 10px;
	}

	a.readmore:hover{
		background: none repeat scroll 0% 0% #567190;
		color: #FFFFFF;
	}

</style>
</head>

<body>
	<script>
dataLayer = [];
</script>

<!-- Google Tag Manager -->
<noscript><ifr ame src="//www.googletagmanager.com/ns.html?id=GTM-TBMKFV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TBMKFV');</script>
<!-- End Google Tag Manager -->
	<div class="container">
		<a href="https://www.vzv.su/polezno-znat/"><img src="https://www.vzv.su/images/logo.png" title="Компания VZV.su" alt="Компания VZV.su" class="logo" height="47" width="173"></a>
		<div class="sllogo">Быстро - <span>Надежно</span> - Удобно</div>
		<h1>Спасибо!</h1>
		<p>Благодарим вас за проявленный интерес к данному материалу.</p>
		<p>Ссылка на скачивание поступит на указанный e-mail адрес.</p>
		<div class="rm">
		<a class="readmore" href="https://www.vzv.su/polezno-znat/" rel="bookmark" title="Назад к статьям в блог"> &laquo; Назад к статьям</a>
	    </div>
	</div>
	
<div style="display:none;">	
<?php

$mail=$_POST["Email"];

$request ='<ApiRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xs="http://www.w3.org/2001/XMLSchema">';
  $request .='<ApiKey>IzurTwEA7u4ThQ5i802w</ApiKey>';
  $request .='<Data xsi:type="Subscriber">';
    $request .='<Mode>AddAndUpdate</Mode>';
    $request .='<Force>false</Force>';
    $request .='<ListId>32</ListId>';
    $request .='<Email>'.$mail.'</Email>';
    $request .='<Properties>';
      $request .='<Property>';
        $request .='<Id>49</Id>';
        $request .='<Value xsi:type="xs:boolean">true</Value>';
      $request .='</Property>';
    $request .='</Properties>';
  $request .='</Data>';
$request .='</ApiRequest>';

$curl_options = array (
  CURLOPT_URL => 'https://api2.esv2.com/Api/Subscribers/',
  CURLOPT_POST => TRUE,
  CURLOPT_RETURNTRANSFER => FALSE,
  CURLOPT_HEADER => array(
        'POST https://api2.esv2.com/Api/Subscribers/ HTTP/1.1', 
        'Accept-Encoding: gzip,deflate', 
        'Content-Type: text/xml', 
        'User-Agent: Jakarta Commons-HttpClient/3.1',
        'Host: api.esv2.com',
        'Content-Length: 715'
    ),
  CURLOPT_POSTFIELDS => ($request)
  
);
$curl = curl_init() or die("cURL init error");
curl_setopt_array($curl, $curl_options) or die("cURL set options error" . curl_error($curl));
$response = curl_exec($curl) or die ("cURL execute eroor" . curl_error($curl));
print_r($response);
curl_close($curl);
?>
</div>
</body>
</html>
