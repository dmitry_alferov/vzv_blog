jQuery.extend(verge);
var desktop = true,
    tablet = false,
    tabletPortrait = false,
    mobile = false;

$(window).resize(function () {
    if ($.viewportW() > 1203) {
        desktop = true;
        tablet = false;
        tabletPortrait = false;
        mobile = false;
    }
    if ($.viewportW() >= 768 && $.viewportW() <= 1203) {
        desktop = false;
        tablet = true;
        tabletPortrait = false;
        mobile = false;
    }
    if ($.viewportW() <= 767) {
        desktop = false;
        tablet = false;
        tabletPortrait = false;
        mobile = true;
    }

}).resize();
function setEqualHeight(columns) {
    var tallestcolumn = 0;
    columns.each(
        function () {
            currentHeight = $(this).height();
            if (currentHeight > tallestcolumn) {
                tallestcolumn = currentHeight;
            }
        }
    );
    columns.height(tallestcolumn);
}

$(function () {

    $('.filtr__popup .icon_close').on('click', function () {
        $(this).closest('.row').remove();
    });

    $('.m-brand span').on('click', function () {
        $('body').addClass('open-more-brand');
        $('.more-product').mCustomScrollbar();
    });

    $('.more-product .close').on('click', function () {
        $('body').removeClass('open-more-brand');
    });

    $('.catalog-content__sel .item__b').on('click', function () {
        $('.item__b').removeClass('active');
        $(this).addClass('active');
        var index = $(this).index();
        $('.popular-goods-wrap .popular-goods').removeClass('active').eq(index).addClass('active');
        $('.popular-goods .col').css("height", "auto");
        setEqualHeight($('.popular-goods .col'));
    });

    $('.aside-catalog .item span').on('click', function () {
        $(this).closest('.row').find('ul').slideToggle();
        $(this).closest('.item').toggleClass('active');
    });
    $('.aside-catalog .item .close').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.row').find('ul').slideUp();
        $(this).closest('.item').removeClass('active');
    });

    //PRICE SLIDER START//
    var minPrice = 250;
    var maxPrice = 32005;

    var minPriceInp = $('.min');
    var maxPriceInp = $('.max');
    var slider = $(".price-slider");
    minPriceInp.val(minPrice);
    maxPriceInp.val(maxPrice);

    slider.slider({
        range: true,
        min: minPrice,
        max: maxPrice,
        values: [minPrice, maxPrice],
        slide: function (event, ui) {
            minPriceInp.val(ui.values[0]);
            maxPriceInp.val(ui.values[1]);
        }
    });

    minPriceInp.change(function () {
        var low = minPriceInp.val(),
            high = maxPriceInp.val();
        low = Math.min(low, high);
        minPriceInp.val(low);
        slider.slider('values', 0, low);
    });

    maxPriceInp.change(function () {
        var low = minPriceInp.val(),
            high = maxPriceInp.val();
        high = Math.max(low, high);
        maxPriceInp.val(high);
        slider.slider('values', 1, high);
    });

    var input = $('.price-slider input');
    input.on('keydown', function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode == 67 && e.ctrlKey === true) || (e.keyCode == 88 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    var inputNum = $('input.only-numbers');
    inputNum.on('keydown', function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode == 67 && e.ctrlKey === true) || (e.keyCode == 88 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    //PRICE SLIDER END//

    $("body").on("click", function (e) {
        if (!$(e.target).closest(".item_select .title").length
            && !$(e.target).closest(".item_select .drop").length
            && !$(e.target).closest('.cabin-drop').length
            && !$(e.target).closest('.cabin').length) {
            $('.cabin-drop').slideUp();
            $('.item_select .drop').slideUp();
            $('.cabin').removeClass('active');

        }
    });

    if (mobile || tablet) {
        $('body').addClass('hide-hover');
    }

    if (!mobile) {

        $(window).resize(function () {
            setTimeout(function () {
                $('.popular-goods .col').css("height", "auto");
                setEqualHeight($('.popular-goods .col'));
            }, 200);
        }).resize();
    }

    $('#remove-slide').on('click', function (e) {
        e.preventDefault();
        $('.viewed-products').slideUp();
    });

    $('.drop .close').on('click', function () {
        $('.drop').slideUp();
    });

    $('.sign-up').on('click', function (e) {
        e.preventDefault();
        $('body').addClass('check-in-open');
    });
    $('.check-in .close').on('click', function () {
        $('body').removeClass('check-in-open');
    });

    $('.check-rates').on('click', function (e) {
        e.preventDefault();
        if (!$('body').hasClass('popup-price_open')) {
            $('body').addClass('popup-price_open')
        } else {
            $('body').removeClass('popup-price_open')
        }
    });

    $('.popup-price .close').on('click', function () {
        $('body').removeClass('popup-price_open');
    });

    $('#call-b').on('click', function (e) {
        e.preventDefault();
        $('.call-b').slideDown();
    });

    $('.come-h').on('click', function (e) {
        e.preventDefault();
        $('body').addClass('popup-come-open');
    });
    $('.popup-come .close').on('click', function () {
        $('body').removeClass('popup-come-open');
    });

    $('.call-b .close').on('click', function () {
        $('.call-b').slideUp();
    });

    $('.popup-h-info__close').on('click', function (e) {
        $('.popup-h-info').slideUp();
    });

    $('.feed-b .more').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.col').find('.drop').slideDown();
        $(this).closest('.col').addClass('open-drop');
    });

    $('.product-info__main .link a').on('click', function (e) {
        e.preventDefault();
        var $this = $(this).data('scroll');
        var $item = $('div[id=' + $this + ']');
        $('body').animate({
            scrollTop: $item.offset().top
        }, 500);
    });

    $('.filter a').on('click', function (e) {
        e.preventDefault();
    });
    $.datepicker.regional['ru'] = {
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
            'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
        ],
        monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
            'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'
        ],
        dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
        dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        weekHeader: 'Не',
        dateFormat: "dd.mm.yy",
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        showOtherMonths: true,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);

    $(".datepicker").datepicker({
        dateFormat: "dd.mm.yy"
    });
    $(".filter .item").click(function () {
        $(".filter .item").removeClass('active');
        $(this).addClass('active');
    });

    $('.row-slide-t').on('click', function () {
        $(this).closest('.table-b__wrap').find('.drop-s').slideToggle();
        $(this).closest('.table-b__wrap').toggleClass('active');
    });

    $('.personal-area-table .table .close').on('click', function () {
        $(this).closest('.row').slideUp();
    });
    $('.personal-data .close ,.saved-filters .close').on('click', function () {
        $(this).closest('.row').slideUp();
    });

    $('.row-slide-t .close').on('click', function () {
        $(this).closest('.table-b__wrap').slideUp();
    });

    $('#add-details').on('click', function() {
       $('body').addClass('add-details-open');
    });
    $('.add-details .close').on('click', function() {
        $('body').removeClass('add-details-open');
    });

    $('.checkbox-all').change(function () {
        var checkboxes = $(this).closest('.table-b__wrap').find(':checkbox');
        if($(this).is(':checked')) {
            console.log(checkboxes)
            checkboxes.prop('checked', true);
        } else {
            checkboxes.prop('checked', false);
        }
    });

    $('.add-details .tab li').on('click', function() {
        $('.add-details .tab li').removeClass('active');
        $(this).addClass('active');
        var index = $(this).index();
        $('.add-details form').removeClass('active').eq(index).addClass('active');
    });

    if (mobile) {
        $('.mob-row__head').on('click', function () {
            $(this).closest('.mob-row').find('.mob-row__body').slideToggle();
            $(this).closest('.mob-row').toggleClass('active');
        });
    }

    $('.link-open-drop').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $('.mob-drop').slideToggle();
    });

    $('.cabin').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $('.cabin-drop').slideToggle();
    });

    $('.documents-list li').on('click', function () {
        var index = $(this).index();
        $('.documents-list li').removeClass('active');
        $(this).addClass('active');
        $('.documents-table .list').removeClass('active').eq(index).addClass('active');
    });

    var nhlTeams = ['Anaheim Ducks', 'Atlanta Thrashers', 'Boston Bruins', 'Buffalo Sabres', 'Calgary Flames', 'Carolina Hurricanes', 'Chicago Blackhawks', 'Colorado Avalanche', 'Columbus Blue Jackets', 'Dallas Stars', 'Detroit Red Wings', 'Edmonton OIlers', 'Florida Panthers', 'Los Angeles Kings', 'Minnesota Wild', 'Montreal Canadiens', 'Nashville Predators', 'New Jersey Devils', 'New Rork Islanders', 'New York Rangers', 'Ottawa Senators', 'Philadelphia Flyers', 'Phoenix Coyotes', 'Pittsburgh Penguins', 'Saint Louis Blues', 'San Jose Sharks', 'Tampa Bay Lightning', 'Toronto Maple Leafs', 'Vancouver Canucks', 'Washington Capitals'];
    var nbaTeams = ['Atlanta Hawks', 'Boston Celtics', 'Charlotte Bobcats', 'Chicago Bulls', 'Cleveland Cavaliers', 'Dallas Mavericks', 'Denver Nuggets', 'Detroit Pistons', 'Golden State Warriors', 'Houston Rockets', 'Indiana Pacers', 'LA Clippers', 'LA Lakers', 'Memphis Grizzlies', 'Miami Heat', 'Milwaukee Bucks', 'Minnesota Timberwolves', 'New Jersey Nets', 'New Orleans Hornets', 'New York Knicks', 'Oklahoma City Thunder', 'Orlando Magic', 'Philadelphia Sixers', 'Phoenix Suns', 'Portland Trail Blazers', 'Sacramento Kings', 'San Antonio Spurs', 'Toronto Raptors', 'Utah Jazz', 'Washington Wizards'];
    var nhl = $.map(nhlTeams, function (team) {
        return {value: team, data: {category: 'NHL'}};
    });
    var nba = $.map(nbaTeams, function (team) {
        return {value: team, data: {category: 'NBA'}};
    });
    var teams = nhl.concat(nba);

    $('.autocomplete').devbridgeAutocomplete({
        lookup: teams,
        minChars: 1,
        onSelect: function (suggestion) {
            $('#selection').html('You selected: ' + suggestion.value + ', ' + suggestion.data.category);
        },
        showNoSuggestionNotice: true,
        noSuggestionNotice: 'Sorry, no matching results',
        groupBy: 'category'
    });

    $('.plus-minus-btn').click(function (e) {
        e.preventDefault();
        var $button = $(this);
        var oldValue = $button.closest('.counter-widget').find('input').val();

        if ($button.text() == "+") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }
        $button.closest('.counter-widget').find('input').val(newVal);

    });


    $('.tab-head li').on('click', function (e) {
        var index = $(this).index() - 1;
        $('.tab-head li').removeClass('active');
        $(this).addClass('active');
        $('.product-info__main .col').removeClass('active').eq(index).addClass('active');
        proSlider.trigger('initialized.owl.carousel');
        proSlider.trigger('refresh.owl.carousel');
    });

    /**
     * slide product
     */
    $('.wrap-small-images img').on('click', function () {
        var $img = $('.big-img');
        var $src = $(this).attr('src');
        var path = $src.substring($src.lastIndexOf('/'));
        var fileName = $src.substring(0, $src.lastIndexOf('/'));
        var newSrc = fileName + "/large" + path;

        var largeImage = $(this).attr('data-image');
        var smallImage = $(this).attr('src');
        $('.wrap-small-images .img').removeClass('active');

        $img.find('img').attr('src', $src);

        var ez = $('#zoom').data('elevateZoom');
        ez.swaptheimage(largeImage, largeImage);
        $(this).closest('.img').addClass('active');

    });
    $('#zoom').elevateZoom({
        zoomType: "inner",
        cursor: "crosshair",
        zoomWindowFadeIn: 500,
        zoomWindowFadeOut: 500
    });

    $('#dress').on('click', function () {
        $('.wrap-item .drop').slideToggle();
    });

    $('.burger').on('click', function () {
        $('body').toggleClass('open-menu');
    });

    $('.select-inp-header').select2({
        minimumResultsForSearch: -1
    });

    $('.select-inp2').select2({
        minimumResultsForSearch: -1
    });

    function format(state) {
        //console.log(state);
        var originalOption = state.element;
        return "<img class='select-img' src='static/img/content/select/" + $(originalOption).data('img') + ".png'/>" + state.text;
    }


    $(".select-inp").select2({
        minimumResultsForSearch: 99,
        dropdownCssClass: 'colorlist_light',
        selectOnBlur: true,
        formatResult: format,
        formatSelection: format,
        escapeMarkup: function (m) {
            return m;
        }
    });

    if(mobile) {

        var newsSlider = $('.info-post .row');
        if (newsSlider.children().length > 1) {
            newsSlider.on('initialized.owl.carousel', function () {
                newsSlider.css("opacity", 1);
            });

            newsSlider.owlCarousel({
                margin: 0,
                nav: true,
                items: 1
            });

        } else {
            newsSlider.css("opacity", 1);
        }
    }
    var proSlider = $('.slide__main');
    if (proSlider.children().length > 1) {
        proSlider.on('initialized.owl.carousel', function () {
            proSlider.css("opacity", 1);
        });

        proSlider.owlCarousel({
            margin: 0,
            nav: true,
            items: 6
        });

    } else {
        proSlider.css("opacity", 1);
    }


    var proSlider2 = $('.viewed-products__slide');
    if (proSlider2.children().length > 1) {
        proSlider2.on('initialized.owl.carousel', function () {
            proSlider2.css("opacity", 1);
        });

        proSlider2.owlCarousel({
            margin: 0,
            loop: true,
            nav: true,
            smartSpeed: 700,
            items: 3,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                }
            }
        });

    } else {
        proSlider2.css("opacity", 1);
    }
    var counter = 0;

    $('.remove').click(function () {
        counter--;
        proSlider2.data('owlCarousel').removeItem(0);
    });

    var proSlider3 = $('.product-info__img-slider .row');
    if (proSlider3.children().length > 1) {
        proSlider3.on('initialized.owl.carousel', function () {
            proSlider3.css("opacity", 1);
        });

        proSlider3.owlCarousel({
            margin: 0,
            loop: true,
            nav: false,
            autoHeight: true,
            smartSpeed: 700,
            items: 1
        });

    } else {
        proSlider3.css("opacity", 1);
    }

    var proSlider4 = $('.main__top-slide .row');
    if (proSlider4.children().length > 1) {
        proSlider4.on('initialized.owl.carousel', function () {
            proSlider4.css("opacity", 1);
        });

        proSlider4.owlCarousel({
            margin: 0,
            loop: true,
            nav: true,
            smartSpeed: 700,
            items: 1
        });

    } else {
        proSlider4.css("opacity", 1);
    }

    var proSlider4 = $('.delivery__i .row');
    if (proSlider4.children().length > 1) {
        proSlider4.on('initialized.owl.carousel', function () {
            proSlider4.css("opacity", 1);
        });

        proSlider4.owlCarousel({
            margin: 0,
            loop: true,
            nav: true,
            smartSpeed: 700,
            items: 5,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 2
                },
                768: {
                    items: 5
                },
                900: {
                    items: 5
                }
            }
        });

    } else {
        proSlider4.css("opacity", 1);
    }

    var proSlider5 = $('.popular-goods__i .row');
    if (proSlider5.children().length > 1) {
        proSlider5.on('initialized.owl.carousel', function () {
            proSlider5.css("opacity", 1);
        });

        proSlider5.owlCarousel({
            margin: 20,
            loop: true,
            nav: true,
            smartSpeed: 700,
            items: 4,
            autoHeight: true,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1024: {
                    items: 4
                }
            }
        });

    } else {
        proSlider5.css("opacity", 1);
    }


    if (mobile) {
        $('.col-first').on('click', function () {
            $(this).closest('.row').find('.list').slideToggle();
            $(this).toggleClass('active');
        });
    }


    $('.select-inp2').on('change', function () {
        var $this = $(this);
        if ($(this).val() == '2') {
            $this.closest('.step-sel').addClass('open-company');
        } else if ($(this).val() == '3') {
            $this.closest('.step-sel').removeClass('open-company');
            $this.closest('.step-sel').addClass('open-all');
        } else {
            $this.closest('.step-sel').removeClass('open-company');
            $this.closest('.step-sel').removeClass('open-all');
        }
    });

    $('#in-sel li a').on('click', function (e) {
        e.preventDefault();
        $('#in-sel li').removeClass('active');
        $(this).closest('li').addClass('active');
        if ($('.onDoor').hasClass('active')) {
            $('.on-door').addClass('active')
        } else {
            $('.on-door').removeClass('active')
        }
    });

    $('.checkout-list .btn').on('click', function (e) {
        e.preventDefault();

        var $step = $('.step'),
            $li = $('.check-step li');

        var $offsetTop = $('.checkout-main__title').offset().top;
        $('html, body').animate({scrollTop: $offsetTop}, 500);
        if ($('.step_1').hasClass('active')) {
            $step.fadeOut();
            $li.removeClass('active');
            setTimeout(function () {
                $step.removeClass('active');
                $step.eq(1).addClass('active');
                $step.eq(1).fadeIn();
            }, 400);
            $li.eq(1).addClass('active');
        } else if ($('.step_2').hasClass('active')) {
            $step.fadeOut();
            $li.removeClass('active');
            setTimeout(function () {
                $step.removeClass('active');
                $step.eq(2).addClass('active');
                $step.eq(2).fadeIn();
            }, 400);
            $li.eq(2).addClass('active');
        } else if ($('.step_3').hasClass('active')) {
            $step.fadeOut();
            $('.check-step').remove();
            setTimeout(function () {
                $step.removeClass('active');
                $step.eq(3).addClass('active');
                $step.eq(3).fadeIn();
            }, 400);


            proSlider5.trigger('initialized.owl.carousel');
            proSlider5.trigger('refresh.owl.carousel');

        }
    });


    $('.catalog-content__bred-mobile .filter').on('click', function (e) {
        e.preventDefault();
        $('.catalog-content__bred-mobile .sort').removeClass('active');
        $(this).toggleClass('active');
        $('.aside-catalog.mobile-show').slideToggle();
        $('.sort-price').slideUp()
    });
    $('.catalog-content__bred-mobile .sort').on('click', function (e) {
        e.preventDefault();
        $('.catalog-content__bred-mobile .filter').removeClass('active');
        $(this).toggleClass('active');
        $('.aside-catalog.mobile-show').slideUp();
        $('.sort-price').slideToggle()
    });
});

if ($('#map').length) {
    //yandex map
    ymaps.ready(function() {
        var map = new ymaps.Map("map", {
            center: [55.7401,37.7837],
            zoom: 11
        });

        var markers = [];

        function clearOverlays() {
            if (markers) {
                for (i in markers) {
                    map.geoObjects.remove(markers[i]);
                }
            }
            markers = [];
        }

        $.each(contactCoordinates, function (i, el) {
            var marker = new ymaps.Placemark(el.coordinates, el.data, el.icon);
            markers.push(marker);

            map.geoObjects.add(marker);
        });

        map.behaviors.disable("scrollZoom");
    });
}
