<?php /**
 * Template Name: Glossary template
 */
?>
<?php get_header(); ?>
<div class="content">
  <div class="content-top">
    <a href="#" class="item-home desktop-hide">
      <i class="icon icon_home"></i>
    </a>
    <?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(); ?>
    <!-- breadcrumb end-->
  </div>
  <!-- content-top end-->
  <div class="termins">
    <div class="wrap-center">
      <div class="wrap-center__i clearfix">
        <?php get_sidebar(); ?>

        <div class="termins-content">
          <div class="blog__title">
            <div class="blog__title__name">
              <p>
                База знаний компании VZV.su
              </p>
            </div>
            <div class="blog__title__categories">

              <?php wp_nav_menu(array(
                'theme_location' => 'blog-menu',
                'items-wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>'
                )); ?>

              </div>
            </div>
            <div class="termins-content__text">
              <p>
                Термины обувного бизнеса
              </p>
              <p>
                Уважаемые коллеги и те, кто только планирует открыть магазин обуви, здесь вы сможете найти исчерпывающую информацию по терминам обувной промышленности и бизнеса. Выберите термин из интересующих вас рубрик справа в меню, используйте поиск по терминам или
                воспользуйтесь алфавитным указателем справа.
              </p>
              <p>
                Давайте говорить на одном языке!
              </p>
            </div>
            <div class="termins-wrapper">
              <div class="termins-wrapper__title">
                <p>
                  Термины:
                </p>
              </div>
              <div class="termins-wrapper__content">
                <?php $i = 0;
                query_posts('post_type=glossary&nopaging=true&order=ASC&orderby=title');
                if (have_posts()) : ?>  
                <?php while (have_posts()) : the_post(); ?>
                  <span <?php post_class() ?>  id="post-<?php the_ID(); ?>" >
                    <p><?php $thisChar = mb_substr(get_the_title(), 0, 1);
                      if($thisChar != $lastChar){
                        $lastChar = $thisChar;
                        if($lastChar == 'А'){
                          echo'<div class="grid-item">';
                        };
                        $regex = "/Б|В|Г|Д|Е|Ж|З|И|К|Л|М|Н|О|П|Р|С|Т|У|Ф|Х|Ц|Ч|Ш|Щ|Э|Ю|Я/";
                        $num_matches = preg_match($regex, $lastChar);

                        if($num_matches == 1){
                          echo'</div>';
                          echo'<div class="grid-item">';
                        };
                        echo '<h2>' . $lastChar . '</h2>';
                        echo '<h5 class="title">'. $title. '</h5>';
  //$i = 1;

                      } ?></p>
                      <h5 class="title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Постоянная ссылка на <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                      </h5>

                      <?php
                      if ($i == 1){
                        echo'</div>';
                        echo'<div class="one-char-block">';
                      }
                      ?>

                    </span>



                  <?php endwhile; ?>
                </div>

                <div class="navigation">
                  <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } else { ?>
                  <div class="alignleft"><?php next_posts_link('&laquo; Предыдущие записи') ?></div>
                  <div class="alignright"><?php previous_posts_link('Следующие записи &raquo;') ?></div>
                  <?php } ?>
                </div>
              <?php else : ?>
                <h2 class="center">Не найдено.</h2>
                <p class="center">Извините, но по Вашему запросу ничего не было найдено.</p>
                <?php get_search_form(); ?>

              <?php endif; ?>
            </div>
          </div>
        </div>
        <!-- termins-content end-->
      </div>
    </div>
  </div>
  <!-- aricles end-->
</div>
<!-- content end-->

<?php get_footer(); ?>