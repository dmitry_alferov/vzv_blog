<?php 

/**
 * ENQUEUE FILES.
 */
include_once('functions/enqueue-files.php');

/* Breadcrumbs */
include_once('functions/breadcrumbs.php');

/* Custom post types */
include('functions/post-types.php');

// Delete ... for the_excerpt

add_filter('excerpt_more', function($more) {
	return '';
});

?>


