<?php 
/**
 * Enqueue scripts and styles.
 */

function vzv_Blog_scripts() {

	// Styles
	wp_enqueue_style( 'vzv_extend_style', get_template_directory_uri() . '/css/mainvlw3adims4i.min.css', array(), '4.2.12');
	wp_enqueue_style( 'vzv_style', get_stylesheet_uri() );

	// Scripts
	wp_enqueue_script("jquery");
	wp_enqueue_script('mainvlw3adims4i-min-js', get_template_directory_uri() . '/js/mainvlw3adims4i.min.js', array(), '1.0', true);
	wp_enqueue_script('start-js', get_template_directory_uri() . '/js/separate-js/start.js', array(), '1.0', true);

}

add_action( 'wp_enqueue_scripts', 'vzv_Blog_scripts' );

	// Post-thumbnails
add_theme_support( 'post-thumbnails' );

// Menu registration
register_nav_menu( 'blog-menu', 'blog-menu' );
register_nav_menu('about', 'footer-about');
register_nav_menu('catalog', 'footer-catalog');
register_nav_menu('info', 'footer-info');

// Sidebar registration
$args = array(
	'name'          => 'main sidebar',
	'id'						=> 'side',
	'description'   => 'Main sidebar for site',
	'class'         => 'blog-asside',
	'before_widget' => '<div id="%1$s" class="categories">',
	'after_widget'  => '</div>',
	'before_title'  => '<div class="blog-asside__title"><p>',
	'after_title'   => '</p></div>'
	);
register_sidebar( $args );

$args_terms = array(
	'name'          => 'terms sidebar',
	'id'						=> 'tside',
	'description'   => 'terms sidebar',
	'class'         => 'blog-asside',
	'before_widget' => '<div id="%1$s" class="categories">',
	'after_widget'  => '</div>',
	'before_title'  => '<div class="blog-asside__title"><p>',
	'after_title'   => '</p></div>'
	);
register_sidebar( $args_terms );

?>