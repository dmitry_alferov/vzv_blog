<?php
class glossary {
	
	function glossary() {
		add_action('init',array($this,'create_post_type'));
	}
	
	function create_post_type() {
		$labels = array(
		    'name' => 'Термины обувного бизнеса',
		    'singular_name' => 'Термины обувного бизнеса',
		    'add_new' => 'Добавить',
		    'all_items' => 'Все',
		    'add_new_item' => 'Добавить',
		    'edit_item' => 'Изменить',
		    'new_item' => 'Новый',
		    'view_item' => 'Посмотреть',
		    'search_items' => 'Найти',
		    'not_found' =>  'Не найдено',
		    'not_found_in_trash' => 'Не найдено',
		    'parent_item_colon' => 'Вложенность:',
		    'menu_name' => 'Глоссарий'
		);
		$args = array(
			'labels' => $labels,
			'description' => "",
			'public' => true,
			'exclude_from_search' => true,
			'publicly_queryable' => true,
			'show_ui' => true, 
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => 20,
			'menu_icon' => null,
			'capability_type' => 'post',
			'hierarchical' => true,
			'supports' => array('title','editor'),
			'has_archive' => true,
			'rewrite' => array('slug' => 'glossary', 'with_front' => 'glossary'),
			'query_var' => true,
			'can_export' => true,
		); 
		register_post_type('glossary',$args);
	}
}
				
function glossary_cat() {
   $labels = array(
      'name'              => _x( 'Категории', 'taxonomy general name' ),
      'singular_name'     => _x( 'Категория', 'taxonomy singular name' ),
      'search_items'      => __( 'Найти категорию' ),
      'all_items'         => __( 'Все категории' ),
      'parent_item'       => __( 'Родительская категория' ),
      'parent_item_colon' => __( 'Родительская категория:' ),
      'edit_item'         => __( 'Редактировать категорию' ), 
      'update_item'       => __( 'Обновить категорию' ),
      'add_new_item'      => __( 'Добавить новую категорию' ),
      'new_item_name'     => __( 'Новая категория' ),
      'menu_name'         => __( 'Категории' ),
   );
   $args = array(
      'labels' => $labels,
      'hierarchical' => true,
	  'show_in_nav_menus' => true,
	  'rewrite' => array( 'slug' => 'glossary_category' ),
			'show_in_menu' => true,
   );
   register_taxonomy( 'glossary_cats', array('glossary'), $args );
}
add_action( 'init', 'glossary_cat', 0 );
function glossary_alf() {
   $labels = array(
      'name'              => _x( 'Алфавит', 'taxonomy general name' ),
      'singular_name'     => _x( 'Буква', 'taxonomy singular name' ),
      'search_items'      => __( 'Найти букву' ),
      'all_items'         => __( 'Все буквы' ),
      'parent_item'       => __( 'Родительская буква' ),
      'parent_item_colon' => __( 'Родительская буква:' ),
      'edit_item'         => __( 'Редактировать букву' ), 
      'update_item'       => __( 'Обновить букву' ),
      'add_new_item'      => __( 'Добавить новую букву' ),
      'new_item_name'     => __( 'Новая буква' ),
      'menu_name'         => __( 'Буквы' ),
   );
   $args = array(
      'labels' => $labels,
      'hierarchical' => true,
	  'show_in_nav_menus' => true,
	  'rewrite' => array( 'slug' => 'alf' ),
			'show_in_menu' => true,
   );
   register_taxonomy( 'alf', array('glossary'), $args );
}
add_action( 'init', 'glossary_alf', 0 );
$glossary = new glossary();

?>