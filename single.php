<?php get_header(); ?>
<div class="content">
  <div class="content-top">
    <a href="#" class="item-home desktop-hide">
      <i class="icon icon_home"></i>
    </a>
    <?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(); ?>
    <!-- breadcrumb end-->
  </div>
  <!-- content-top end-->
  <div class="articles">
    <div class="wrap-center">
      <div class="wrap-center__i clearfix">
        <?php get_sidebar(); ?>
        <!-- blog-asside end-->
        <div class="articles-content">
          <div class="blog__title">
            <div class="blog__title__name">
              <p>
                База знаний компании VZV.su
              </p>
            </div>
            <div class="blog__title__categories">

              <?php wp_nav_menu(array(
                'theme_location' => 'blog-menu',
                'items-wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>'
                )); ?>

              </div>
            </div>
            <!-- blog__title end-->
            <div class="article">
              <div class="article__prewiew">
                <div class="article__prewiew__image" 
                style="background-image:url('<?php
                 if ( has_post_thumbnail()) {
                  $full_image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
                  echo "".$full_image_url[0] . "";
                }
                ?>')">
              </div>
              <div class="article__prewiew__tech-info">
                <ul>
                  <li>
                    <i class="icon icon-person"></i>
                    <?php the_author(); ?>
                  </li>
                  <li>
                    <i class="icon icon-calendar"></i>
                    <?php the_date('j F Y'); ?>
                  </li>
                  <li>
                    <i class="icon icon-quote"></i>
                    <?php comments_number('Нет комментариев', '1', '%'); ?>
                  </li>
                </ul>
              </div>
              <div class="article__prewiew__name-article">
                <p>
                  <?php the_title(); ?>
                </p>
              </div>
             <!--  <div class="article__prewiew__prev-article">
                <a href="#"><i class="icon icon-prev"></i> Предыдущая статья</a>
              </div>
              <div class="article__prewiew__next-article">
                <a href="#">Следующая статья <i class="icon icon-next"></i></a>
              </div> -->
              <?php 
              the_post_navigation( array(
                'prev_text' => '<div class="article__prewiew__prev-article">
                <span><i class="icon icon-prev"></i> Предыдущая статья</span></div>',
                'next_text' => '<div class="article__prewiew__next-article">
                <span>Следующая статья <i class="icon icon-next"></i></span>
              </div>',
              'screen_reader_text' => ' ',
              )); ?>
            </div>
            <div class="post">
              <?php the_content(); ?>
            </div>

            <div class="article__conclusions">
              <div class="article__conclusions__title">
                <p>
                  Выводы
                </p>
              </div>
              <div class="article-author clearfix">
                <div class="col-left">
                  <div class="author-photo">
                    <?php
                    $author = get_the_author_meta('email');
                    echo get_avatar($author);
                    ?>
                    <p>Автор статьи - <span><?php the_author(); ?> </span>
                    </p>
                  </div>
                  <div class="ask-author">
                    <a href="#" class="btn btn_red">

                     Задать вопрос автору

                   </a>
                 </div>
               </div>
               <div class="col-right">
                <?php echo get_the_author_meta('description'); ?>
              </div>
            </div>
          </div>
          <div class="article__share clearfix">
            <div class="socialls">
              <p>
                Поделитесь статьей:
              </p>
              <a href="" class="color-vk">
                <i class="icon icon-vk">

                </i>
              </a>
              <a href="" class="color-fb">
                <i class="icon icon-fb">

                </i>
              </a>
              <a href="" class="color-tw">
                <i class="icon icon-tw">

                </i>
              </a>
              <a href="" class="color-od">
                <i class="icon icon-od">			

                </i>
              </a>
            </div>
            <div class="good-or-bad">
              <a href="#" class="btn btn-good btn_blue-bg">

               Полезно <span>124</span>

             </a>
             <a href="#" class="btn btn-bad">

               Не полезно <span>2</span>

             </a>
           </div>
         </div>
         <div class="article__comments">
          <div class="article__comments__title">
            <p>
              Комментарии:
            </p>
          </div>
          <div class="article__comments__wrap">
            <div class="comment">
              <div class="comment__inner">
                <div class="comment-rating">
                  <i class="icon icon-star-good"></i>
                  <i class="icon icon-star-good"></i>
                  <i class="icon icon-star"></i>
                  <i class="icon icon-star"></i>
                  <i class="icon icon-star"></i>
                </div>
                <div class="comment-author">
                  <p>
                    Александр Дорвеев
                  </p>
                </div>
                <div class="comment-text">
                  <p>
                    Отличная статья, оставлю в заметках, будет полезно!
                  </p>
                </div>
              </div>
              <div class="comment__reply">
                <div class="comment__reply__item">
                  <div class="reply-author">
                    <p>
                      Властелин Марков
                    </p>
                  </div>
                  <div class="reply-text">
                    <p>
                      Поддерживаю!
                    </p>
                  </div>
                </div>
                <div class="comment__reply__item">
                  <div class="reply-author">
                    <p>
                      Юрий Буцман
                    </p>
                  </div>
                  <div class="reply-text">
                    <p>
                      В статье правы, но есть нюансы
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div class="comment">
              <div class="comment__inner">
                <div class="comment-rating">
                  <i class="icon icon-star-good"></i>
                  <i class="icon icon-star-good"></i>
                  <i class="icon icon-star-good"></i>
                  <i class="icon icon-star-good"></i>
                  <i class="icon icon-star-good"></i>
                </div>
                <div class="comment-author">
                  <p>
                    Властелин Марков
                  </p>
                </div>
                <div class="comment-text">
                  <p>
                    Мне понравилось, только на счет производства в Греции вы не правы
                  </p>
                </div>
                <a href="#" class="btn btn-reply">Ответить</a>
              </div>
            </div>
            <div class="comment">
              <div class="comment__inner">
                <div class="comment-rating">
                  <i class="icon icon-star-good"></i>
                  <i class="icon icon-star-good"></i>
                  <i class="icon icon-star-good"></i>
                  <i class="icon icon-star-good"></i>
                  <i class="icon icon-star-good"></i>
                </div>
                <div class="comment-author">
                  <p>
                    Юрий Буцман
                  </p>
                </div>
                <div class="comment-text">
                  <p>
                    Хорошо сделано, согласен с вами, но стоит ли тогда запускать 3 часть?
                  </p>
                </div>
              </div>
            </div>
            <div class="comment">
              <div class="comment__inner">
                <div class="comment-rating">
                  <i class="icon icon-star-good"></i>
                  <i class="icon icon-star-good"></i>
                  <i class="icon icon-star"></i>
                  <i class="icon icon-star"></i>
                  <i class="icon icon-star"></i>
                </div>
                <div class="comment-author">
                  <p>
                    Александр Дорвеев
                  </p>
                </div>
                <div class="comment-text">
                  <p>
                    Отличная статья, оставлю в заметках, будет полезно!
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="article__add-comment">
          <div class="article__add-comment__title">
            <p>
              Оставьте свой комментарий
            </p>
          </div>
          <form action="">
            <div class="row clearfix">
              <div class="input">
                <input type="text" placeholder="Ваше имя">
              </div>
              <div class="input">
                <input type="text" placeholder="Email">
              </div>
            </div>
            <div class="row">
              <textarea name="" id="" placeholder="Введите ваше сообщение"></textarea>
            </div>
            <div class="row clearfix">
              <div class="estimate">
                <p>
                  Оцените статью
                </p>
                <div class="estimate__items">
                  <a href="#" class="icon icon-star"></a>
                  <a href="#" class="icon icon-star"></a>
                  <a href="#" class="icon icon-star"></a>
                  <a href="#" class="icon icon-star"></a>
                  <a href="#" class="icon icon-star"></a>
                </div>
              </div>
              <div class="submit">
                <input type="submit" class="btn btn_blue-bg" value="Оставить комментарий">
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- article end-->
    </div>
    <!-- articles-content end-->
  </div>
</div>
</div>
<!-- aricles end-->
</div>
<!-- content end-->
<?php get_footer(); ?>