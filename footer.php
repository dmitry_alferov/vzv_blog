<footer class="page__footer">
  <div class="wrap-center">
    <div class="wrap-center__i">
      <div class="main-tel">
        <a href="tel:88005551964" class="tel"><b>8 (800)</b> 555-19-64</a>

        <p>Звонок по России бесплатный</p>
        <div class="mobile-show">
          <a href="#" class="btn btn_light btn_light-big">Заказать обратный звонок</a>
        </div>
      </div>
      <!-- main-tel end-->
      <div class="footer__info-t">
        <div class="col">
          <p class="footer__menu-header">О компании</p>
          <?php wp_nav_menu(array(
            'theme_location' => 'about',
            'container' => 'false'
          )); ?>
          
        </div>
        <div class="col">
          <p class="footer__menu-header">Каталог</p>
          <?php wp_nav_menu(array(
            'theme_location' => 'catalog',
            'container' => 'false'
          )); ?>
          
        </div>
        <div class="col">
        <p class="footer__menu-header">Информация</p>
          <?php wp_nav_menu(array(
            'theme_location' => 'info',
            'container' => 'false'
          )); ?>
         
        </div>
        <div class="col">
          <div class="social">
            <p>Мы в социальных сетях:</p>
            <div class="item">
              <a href="#" class="color-vk"><i class="icon icon_vk"></i></a>
              <a href="#" class="color-f"><i class="icon icon_fac"></i></a>
            </div>
            <div class="item">
              <a href="#" class="color-tw"><i class="icon icon_tw"></i></a>
              <a href="#" class="color-odn"><i class="icon icon_odn"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer__info-b">
    <div class="wrap-center">
      <div class="wrap-center__i">
        <div class="info-address">
          <div class="mobile-show">
            <a href="#" class="link-open-drop">Пункты выдачи товаров <i class="icon icon_arr-r-black"></i></a>
          </div>
          <div class="mob-drop">
            <div class="title">
              <span>Пункт выдачи товаров</span>
            </div>
            <div class="row">
              <div class="col">
                <b>Москва</b>

                <p>Адрес офиса: Россия, Московская область, г. Москва, ул. Кусковская, д. 20 А, офис А-502
                </p>

                <p>Адрес склада: Россия, Московская область, г. Москва, ул. Автозаводская, д. 23, стр. 7 Телефон: 8 (800) 555-19-64</p>
              </div>
              <div class="col">
                <b>Самара</b>

                <p>Адрес: Россия, Самарская область, г. Самара, пр. Мальцева, д. 4</p>

                <p>Телефон: +7 (846) 206-01-33</p>
              </div>
              <div class="col">
                <b>Краснодар</b>

                <p>Адрес: Россия, Краснодарский край, г. Краснодар, ул. Бульварная, д. 2/2
                </p>

                <p>Телефон: +7 (861) 203-36-61</p>
              </div>
              <div class="col">
                <b>Екатеринбург</b>
                <p>Адрес: Россия, Свердловская область, г. Екатеринбург, ул. Норильская, д. 77
                </p>
                <p>Телефон: +7 (343) 318-01-53</p>
              </div>
              <div class="col">
                <b>Казань</b>
                <p>Адрес: Россия, Республика Татарстан, г. Казань, ул. Техническая, д. 52 а
                </p>
                <p>Телефон: +7 (843) 206-01-52</p>
              </div>
              <div class="col">
                <b>Волгоград</b>
                <p>Адрес: Россия, Волгоградская область, г. Волгоград, ул. Землячки, д. 16
                </p>
                <p>Телефон: +7 (844) 278-03-86</p>
              </div>
            </div>
          </div>

        </div>
        <div class="footer-text">
          <b>Детская обувь оптом от производителей России и зарубежья</b>
          <p>Детство - это не только самый счастливый и беззаботный период в жизни, а еще и очень ответственные годы роста и развития организма, от которых зависит здоровье человека на протяжении всех лет его жизни. Каждый родитель стремится
            создать для своего чада максимально комфортные условия. С этой целью мамы и папы очень щепетильно выбирают детскую обувь, которая влияет на формирование стопы. Сегодня у покупателей есть возможность выбирать варианты из
            огромного числа товаров, а розничным реализаторам предстоит непростая задача - подобрать для себя варианты поставки в магазин детской обуви, чтобы удовлетворить запросы своих клиентов.
          </p>
        </div>
        <!-- footer-text end-->
        <div class="copy">
          <span>© <?php echo date('Y') ?> VZV.su Все права защищены.Полное или частичное копирование материалов запрещено.</span>
        </div>
      </div>
    </div>
  </div>
  <!--footer__info-b end-->
</footer>

<?php wp_footer(); ?>
</body>

</html>